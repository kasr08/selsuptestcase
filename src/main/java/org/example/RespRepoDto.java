package org.example;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;
import java.util.UUID;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class RespRepoDto {

    /**
     * Идентификатор ответа
     */
    @NotNull(message = "No required attribute specified msgId")
    @NotBlank(message = "No required attribute specified msgId")
    private UUID msgId;


    /**
     * Дата и время ответа
     */
    @NotNull(message = "No required attribute specified msgTime")
    @NotBlank(message = "No required attribute specified msgTime")
    private LocalDateTime msgTime;


    /**
     * Система-источник ответа
     */
    @NotNull(message = "No required attribute specified source")
    @NotBlank(message = "No required attribute specified source")
    private String source;


    /**
     * Тип ответа
     */
    @NotNull(message = "No required attribute specified msgType")
    @NotBlank(message = "No required attribute specified msgType")
    private String msgType;


    /**
     * Номер исходного запроса
     */
    private UUID reqNumber;


    @NotNull(message = "No required attribute specified body")
    private Body body;


    @Data
    public static class Body {


        /**
         * Общее число пачек
         */
        @NotNull(message = "No required attribute specified totalPages")
        @NotBlank(message = "No required attribute specified totalPages")
        private Integer totalPages;


        /**
         * Порядковый номер текущей пачки
         */
        @NotNull(message = "No required attribute specified currentPage")
        @NotBlank(message = "No required attribute specified currentPage")
        private Integer currentPage;


        /**
         * Код клиента
         */
        @NotNull(message = "No required attribute specified mdmId")
        @NotBlank(message = "No required attribute specified mdmId")
        private String mdmId;

        /**
         * Номер договора обслуживания клиента
         */
        @NotNull(message = "No required attribute specified clientContractCode")
        @NotBlank(message = "No required attribute specified clientContractCode")
        private String clientContractCode;


        @NotNull(message = "No required attribute specified records")
        private List<Record> records;

    }

    @Data
    public static class Record {


        /**
         * Направление операции
         */
        @NotNull(message = "No required attribute specified direction")
        private Integer direction;
        /**
         * Элемент
         */
        @NotNull(message = "No required attribute specified element")
        private List<Element> element;

    }

    @Data
    public static class Element {

        /**
         * Дата заключения сделки РЕПО
         */
        @NotNull(message = "No required attribute specified operDate")
        private LocalDateTime operDate;

        /**
         * № сделки РЕПО
         */
        @NotNull(message = "No required attribute specified num")
        private String num;

        /**
         * Выпуск
         */
        @NotNull(message = "No required attribute specified assetId")
        private String assetId;

        /**
         * Значение обращаемости актива
         */
        private Integer codeId;

        /**
         * Количество ЦБ
         */
        private BigDecimal qty;

        /**
         * Пара
         */
        @NotNull(message = "No required attribute specified pair")
        private List<Pair> pair;


        /**
         * Доход / Расход
         */
        @NotNull(message = "No required attribute specified result")
        private BigDecimal result;

        /**
         * Комиссионные затраты (транзактные)
         */
        @NotNull(message = "No required attribute specified sumComm")
        private BigDecimal sumComm;

        /**
         * Доход / Расход с учетом предела (в том числе комисс. затраты (транзактные)
         */
        @NotNull(message = "No required attribute specified okResult")
        private BigDecimal okResult;


    }

    @Data
    public static class Pair {

        /**
         * Направление
         */
        @NotNull(message = "No required attribute specified buySell")
        @NotBlank(message = "No required attribute specified buySell")
        private String buySell;


        /**
         * Дата исполнения
         */
        @NotNull(message = "No required attribute specified exDate")
        private LocalDateTime exDate;


        /**
         * Цена
         */
        private BigDecimal price;

        /**
         * НКД
         */
        private BigDecimal nkd;


        /**
         * Сумма продажи/покупки
         */
        @NotNull(message = "No required attribute specified sum")
        private BigDecimal sum;
    }
}
