package org.example;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClients;


import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.concurrent.atomic.AtomicLong;
import java.util.stream.IntStream;

public class CrptApi {
    private final int requestLimit;
    private final AtomicLong lastRequestTime;
    private final AtomicInteger requestCount;
    private final TimeUnit timeUnit;

    public CrptApi(TimeUnit timeUnit, int requestLimit) {
        this.timeUnit = timeUnit;
        this.requestLimit = requestLimit;
        this.requestCount = new AtomicInteger(0);
        this.lastRequestTime = new AtomicLong(Instant.now().toEpochMilli());
    }

    public boolean isAllowed() {
        long currentTime = Instant.now().toEpochMilli();
        long lastTime = lastRequestTime.get();

        if (currentTime - lastTime > timeUnit.toMillis(1)) {
            requestCount.set(0);
            lastRequestTime.set(currentTime);
        }

        int count = requestCount.getAndIncrement();
        return count < requestLimit;
    }

    private CrptDocument createTestCrptDocument() {
        CrptDocument crptDocument = new CrptDocument();
        System.out.println("все четко");

        CrptDocument.Description description = new CrptDocument.Description();
        description.setParticipantInn("testParticipantInn");
        crptDocument.setDescription(description);

        crptDocument.setDocId("testDocId");
        crptDocument.setDocStatus("testDocStatus");
        crptDocument.setDocType("LP_INTRODUCE_GOODS");
        crptDocument.setImportRequest(true);
        crptDocument.setOwnerInn("testOwnerInn");
        crptDocument.setParticipantInn("testParticipantInn");
        crptDocument.setProducerInn("testProducerInn");

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        try {
            crptDocument.setProductionDate(dateFormat.parse("2020-01-23"));
            crptDocument.setRegDate(dateFormat.parse("2020-01-23"));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        crptDocument.setProductionType("testProductionType");

        CrptDocument.Product product = new CrptDocument.Product();
        product.setCertificateDocument("testCertificateDocument");

        try {
            product.setCertificateDocumentDate(dateFormat.parse("2020-01-23"));
            product.setProductionDate(dateFormat.parse("2020-01-23"));
        } catch (ParseException e) {
            e.printStackTrace();
        }


        product.setCertificateDocumentNumber("testCertificateDocumentNumber");
        product.setOwnerInn("testOwnerInn");
        product.setProducerInn("testProducerInn");
        product.setTnvedCode("testTnvedCode");
        product.setUitCode("testUitCode");
        product.setUituCode("testUituCode");

        crptDocument.setProducts(List.of(product));

        crptDocument.setRegNumber("testRegNumber");

        return crptDocument;
    }

    public void createDocument(CrptDocument crptDocument, String signature) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            String json2;

            try {
                json2 = objectMapper.writeValueAsString(crptDocument);
            } catch (JsonProcessingException e) {
                throw new RuntimeException(e);
            }

            String apiUrl1 = "http://localhost:8080/api/v1/nsi/trueZnak/";
            HttpPost post = new HttpPost(apiUrl1);

            post.setEntity(new StringEntity(String.format("%s, \"signature\":\"%s\"}", json2, signature),
                    "application/json", "UTF-8"));
            HttpClient httpClient = HttpClients.createDefault();

            HttpResponse response = httpClient.execute(post);
            System.out.println(response.getStatusLine());

            System.out.println("ВСЕ ВЫПОЛНИЛОСЬ " + Instant.now());

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    public static class CrptDocument {
        @JsonProperty("description")
        private Description description;

        @JsonProperty("doc_id")
        private String docId;

        @JsonProperty("doc_status")
        private String docStatus;

        @JsonProperty("doc_type")
        private String docType;

        @JsonProperty("importRequest")
        private boolean importRequest;

        @JsonProperty("owner_inn")
        private String ownerInn;

        @JsonProperty("participant_inn")
        private String participantInn;

        @JsonProperty("producer_inn")
        private String producerInn;

        @JsonProperty("production_date")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        private Date productionDate;

        @JsonProperty("production_type")
        private String productionType;

        @JsonProperty("products")
        private List<Product> products;

        @JsonProperty("reg_date")
        @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
        private Date regDate;

        @JsonProperty("reg_number")
        private String regNumber;

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        public static class Description {
            @JsonProperty("participantInn")
            private String participantInn;
        }

        @Data
        @AllArgsConstructor
        @NoArgsConstructor
        public static class Product {
            @JsonProperty("certificate_document")
            private String certificateDocument;

            @JsonProperty("certificate_document_date")
            @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
            private Date certificateDocumentDate;

            @JsonProperty("certificate_document_number")
            private String certificateDocumentNumber;

            @JsonProperty("owner_inn")
            private String ownerInn;

            @JsonProperty("producer_inn")
            private String producerInn;

            @JsonProperty("production_date")
            @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "yyyy-MM-dd")
            private Date productionDate;

            @JsonProperty("tnved_code")
            private String tnvedCode;

            @JsonProperty("uit_code")
            private String uitCode;

            @JsonProperty("uitu_code")
            private String uituCode;
        }
    }

    public static void main(String[] args) {
        CrptApi crptApi = new CrptApi(TimeUnit.MINUTES, 10);
        CrptDocument c = crptApi.createTestCrptDocument();

        IntStream.range(0, 100)
                .peek(i -> {
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                })
                .forEach(i -> {
                    if (crptApi.isAllowed()) {
                        crptApi.createDocument(c, "signature");
                        System.out.println("Sending request to https://ismp.crpt.ru/api/v3/lk/documents/create");
                    } else {
                        System.out.println("Request limit exceeded. Please wait");
                    }
                });

    }
}